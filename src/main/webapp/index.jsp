<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Index Page</title>
</head>
<body>

<c:set var="home" scope="application">/servlets/HomeServlet</c:set>
<c:set var="commonComponents" scope="application">/WEB-INF/jsp/private/common_components.jsp</c:set>
<c:set var="allItems" scope="application">/WEB-INF/jsp/private/items.jsp</c:set>
<c:set var="allMenus" scope="application">/WEB-INF/jsp/private/menus.jsp</c:set>

<c:set var="logoutServlet" scope="application"><c:url value="/servlets/LogoutServlet"/></c:set>
<c:set var="loginServlet" scope="application"><c:url value="/LoginServlet"/></c:set>
<c:set var="homeServlet" scope="application"><c:url value="/servlets/HomeServlet"/></c:set>
<c:set var="manipulateItemsServlet" scope="application"><c:url value="/servlets/admin/ManipulateItemsServlet"/></c:set>
<c:set var="createItemServlet" scope="application"><c:url value="/servlets/admin/CreateItemServlet"/></c:set>
<c:set var="updateItemServlet" scope="application"><c:url value="/servlets/admin/UpdateItemServlet"/></c:set>
<c:set var="getNewItemNameServlet" scope="application"><c:url value="/servlets/admin/GetNewItemNameServlet"/></c:set>
<c:set var="deleteItemServlet" scope="application"><c:url value="/servlets/admin/DeleteItemServlet"/></c:set>
<c:set var="manipulateMenusServlet" scope="application"><c:url value="/servlets/admin/ManipulateMenusServlet"/></c:set>
<c:set var="createMenuServlet" scope="application"><c:url value="/servlets/admin/CreateMenuServlet"/></c:set>
<c:set var="deleteMenuServlet" scope="application"><c:url value="/servlets/admin/DeleteMenuServlet"/></c:set>
<c:set var="viewItemsServlet" scope="application"><c:url value="/servlets/ViewItemsServlet"/></c:set>
<c:set var="viewMenusServlet" scope="application"><c:url value="/servlets/ViewMenusServlet"/></c:set>

<c:set var="errorServlet" scope="application"><c:url value="/servlets/ErrorServlet"/></c:set>

<c:if test="${sessionScope.user!=null}">
    <c:redirect url="${home}"/>
</c:if>

<h1>Please login</h1>

<form action="${applicationScope.loginServlet}" method="post">
    Username: <input type="text" name="username"><br><br>
    Password: <input type="password" name="password"><br><br>
    <input type="submit" value="Login">
</form>
</body>
</html>
