<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Menus</title>
</head>

<body>
<h3>Current Menu</h3>
<c:forEach items="${applicationScope.menu}" var="menu">
    <c:if test="${menu!=null}">
        <c:out value="${applicationScope.intToDay[menu.day]}"/>
        <c:out value="${applicationScope.intToServingType[menu.servingType]}"/>
        <c:if test="${sessionScope.user.type=='admin'}">
            <form action="${applicationScope.deleteMenuServlet}" method="post" style="display: inline-block">
                <input type="hidden" name="deleteMenuDay" value="${menu.day}">
                <input type="hidden" name="deleteMenuServingType" value="${menu.servingType}">
                <input type="submit" value="Delete">
            </form>
        </c:if>
        <ol>
            <c:forEach items="${menu.itemSet}" var="item">
                <li>
                    <c:out value="${item.name}"/>
                </li>
            </c:forEach>
        </ol>
    </c:if>
</c:forEach>
</body>
</html>
