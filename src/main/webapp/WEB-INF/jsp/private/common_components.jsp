<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Common Components</title>
</head>

<body>
<h1>Welcome <c:out value="${user.username}"/></h1>

<form action="${applicationScope.logoutServlet}" method="post">
    <input type="submit" value="Logout">
</form>

<h2><a href="${applicationScope.homeServlet}">Home</a></h2>
</body>
</html>
