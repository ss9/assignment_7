<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Items</title>
</head>

<body>
<h3>Current items</h3>
<ol>
    <c:forEach items="${applicationScope.items}" var="item">
        <li>
            <c:out value="${item.name}"/>
            <c:if test="${sessionScope.user.type=='admin'}">
                <form action="${applicationScope.getNewItemNameServlet}" method="post" style="display: inline-block">
                    <input type="hidden" name="itemIdPassed" value="${item.id}">
                    <input type="submit" value="Update">
                </form>
                <form action="${applicationScope.deleteItemServlet}" method="post" style="display: inline-block">
                    <input type="hidden" name="itemId" value="${item.id}">
                    <input type="submit" value="Delete">
                </form>
            </c:if>
        </li>
    </c:forEach>
</ol>
</body>
</html>
