<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Items</title>
</head>

<body>
<c:import url="${applicationScope.commonComponents}"/>

<c:import url="${applicationScope.allItems}"/>
</body>
</html>
