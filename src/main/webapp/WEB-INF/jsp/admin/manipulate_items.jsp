<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manipulate Items</title>
</head>

<body>
<c:import url="${applicationScope.commonComponents}"/>

<c:import url="${applicationScope.allItems}"/>

<h3>Create Item</h3>

<form action="${applicationScope.createItemServlet}" method="post">
    Item Name: <input type="text" name="itemName" required>
    <input type="submit" value="Create Item">
</form>
</body>
</html>
