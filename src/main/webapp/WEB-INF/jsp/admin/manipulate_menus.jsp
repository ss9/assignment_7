<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manipulate Menus</title>
</head>

<body>
<c:import url="${applicationScope.commonComponents}"/>

<c:import url="${applicationScope.allMenus}"/>

<h3>Create/Update Menu</h3>
<h4>Select the day, serving type and items of the menu</h4>

<form action="${applicationScope.createMenuServlet}" method="post">
    <select name="createMenuDay">
        <c:forEach items="${applicationScope.days}" var="day">
            <option value="${day}"><c:out value="${applicationScope.intToDay[day]}"/></option>
        </c:forEach>
    </select>
    <select name="createMenuServingType">
        <c:forEach items="${applicationScope.servingTypes}" var="servingType">
            <option value="${servingType}"><c:out value="${applicationScope.intToServingType[servingType]}"/></option>
        </c:forEach>
    </select><br><br>
    <c:forEach items="${applicationScope.items}" var="item">
        <input type="checkbox" name="createMenuItem" value="${item.id}">
        <c:out value="${item.name}"/>
    </c:forEach>
    <br><br>
    <input type="submit" value="Create/Update Menu">
</form>
</body>
</html>
