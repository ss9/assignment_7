<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Get New Item Name</title>
</head>

<body>
<c:import url="${applicationScope.commonComponents}"/>

<h3>Type new item name</h3>

<form action="${applicationScope.updateItemServlet}" method="post">
    <input type="text" name="newItemName" required>
    <input type="hidden" name="itemId" value="${param.itemIdPassed}">
    <input type="submit" value="Update Item">
</form>
</body>
</html>
