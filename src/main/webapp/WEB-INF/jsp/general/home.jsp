<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>

<body>
<c:import url="${applicationScope.commonComponents}"/>

<c:if test="${sessionScope.user.type=='admin'}">
    <h2><a href="${applicationScope.manipulateItemsServlet}">Create/Update/Delete Items</a></h2>

    <h2><a href="${applicationScope.manipulateMenusServlet}">Create/Update/Delete Menus</a></h2>
</c:if>

<c:if test="${sessionScope.user.type=='public'}">
    <h2><a href="${applicationScope.viewItemsServlet}">View All Items</a></h2>

    <h2><a href="${applicationScope.viewMenusServlet}">View All Menus</a></h2>
</c:if>
</body>
</html>
