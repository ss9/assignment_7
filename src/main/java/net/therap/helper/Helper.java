package net.therap.helper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shadman
 * @since 11/26/17
 */
public class Helper {

    public static final String USER_TYPE_PUBLIC = "public";
    public static final String USER_TYPE_ADMIN = "admin";

    public static final String INDEX_URL = "/index.jsp";
    public static final String HOME_URL = "/WEB-INF/jsp/general/home.jsp";
    public static final String ERROR_URL = "/WEB-INF/jsp/general/error.jsp";
    public static final String MANIPULATE_ITEMS_URL = "/WEB-INF/jsp/admin/manipulate_items.jsp";
    public static final String MANIPULATE_MENUS_URL = "/WEB-INF/jsp/admin/manipulate_menus.jsp";
    public final static String GET_NEW_ITEM_NAME_URL = "/WEB-INF/jsp/admin/get_new_item_name.jsp";
    public final static String VIEW_ITEMS_URL = "/WEB-INF/jsp/public/view_items.jsp";
    public final static String VIEW_MENUS_URL = "/WEB-INF/jsp/public/view_menus.jsp";

    public static final String CONTENT_TYPE_HTML = "text/html";
    public static final String GENERAL_ERROR_MESSAGE = "An error occurred!";
    public static final String ERROR_IN_USERNAME = "invalid username";
    public static final String ERROR_IN_PASSWORD = "incorrect password";

    public final Map<Integer, String> INT_TO_DAY;
    public final Map<Integer, String> INT_TO_SERVING_TYPE;

    public Helper() {
        INT_TO_DAY = new HashMap<>();
        INT_TO_DAY.put(1, "Sunday");
        INT_TO_DAY.put(2, "Monday");
        INT_TO_DAY.put(3, "Tuesday");
        INT_TO_DAY.put(4, "Wednesday");
        INT_TO_DAY.put(5, "Thursday");
        INT_TO_DAY.put(6, "Friday");
        INT_TO_DAY.put(7, "Saturday");

        INT_TO_SERVING_TYPE = new HashMap<>();
        INT_TO_SERVING_TYPE.put(1, "Breakfast");
        INT_TO_SERVING_TYPE.put(2, "Lunch");
    }
}
