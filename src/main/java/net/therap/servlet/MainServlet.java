package net.therap.servlet;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.dao.UserDao;
import net.therap.domain.Item;
import net.therap.domain.Menu;
import net.therap.domain.User;
import net.therap.helper.Helper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author shadman
 * @since 12/4/17
 */

@Controller
public class MainServlet implements ServletContextAware {

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @RequestMapping(value = "/LoginServlet")
    public String validateUser(@RequestParam String username, @RequestParam String password,
                               HttpServletRequest request) {
        UserDao userDao = new UserDao();
        User user = userDao.getUserByUsername(username);
        if (user == null) {
            return Helper.INDEX_URL;
        } else if (!user.getPassword().trim().equals(password)) {
            return Helper.INDEX_URL;
        } else {
            request.getSession().setAttribute("user", user);
            ItemDao itemDao = new ItemDao();
            MenuDao menuDao = new MenuDao();
            Helper helper = new Helper();
            servletContext.setAttribute("intToDay", helper.INT_TO_DAY);
            servletContext.setAttribute("days", helper.INT_TO_DAY.keySet());
            servletContext.setAttribute("intToServingType", helper.INT_TO_SERVING_TYPE);
            servletContext.setAttribute("servingTypes", helper.INT_TO_SERVING_TYPE.keySet());
            servletContext.setAttribute("items", itemDao.getAllItems());
            servletContext.setAttribute("menu", menuDao.getWeeklyMenu());
            return Helper.HOME_URL;
        }
    }

    @RequestMapping(value = "/servlets/LogoutServlet", method = RequestMethod.POST)
    public String goToIndex(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        request.getSession().invalidate();
        return Helper.INDEX_URL;
    }

    @RequestMapping(value = "/servlets/HomeServlet")
    public String goToHome() {
        return Helper.HOME_URL;
    }

    @RequestMapping(value = "/servlets/ErrorServlet")
    public String goToErrorPage() {
        return Helper.ERROR_URL;
    }

    @RequestMapping(value = "/servlets/ViewItemsServlet")
    public String goToViewItems() {
        return Helper.VIEW_ITEMS_URL;
    }

    @RequestMapping(value = "/servlets/ViewMenusServlet")
    public String goToViewMenus() {
        return Helper.VIEW_MENUS_URL;
    }

    @RequestMapping(value = "/servlets/admin/ManipulateItemsServlet")
    public String goToManipulateItems() {
        return Helper.MANIPULATE_ITEMS_URL;
    }

    @RequestMapping(value = "/servlets/admin/ManipulateMenusServlet")
    public String goToManipulateMenus() {
        return Helper.MANIPULATE_MENUS_URL;
    }

    @RequestMapping(value = "/servlets/admin/CreateItemServlet")
    public String createItem(HttpServletRequest request) {
        String itemName = request.getParameter("itemName");
        Item item = new Item(itemName);
        ItemDao itemDao = new ItemDao();
        MenuDao menuDao = new MenuDao();
        itemDao.addItem(item);
        servletContext.setAttribute("items", itemDao.getAllItems());
        servletContext.setAttribute("menu", menuDao.getWeeklyMenu());
        return Helper.MANIPULATE_ITEMS_URL;
    }

    @RequestMapping(value = "/servlets/admin/CreateMenuServlet", method = RequestMethod.POST)
    public String createMenu(HttpServletRequest request) {
        ItemDao itemDao = new ItemDao();
        MenuDao menuDao = new MenuDao();
        int day = Integer.parseInt(request.getParameter("createMenuDay"));
        int servingType = Integer.parseInt(request.getParameter("createMenuServingType"));
        String[] items = request.getParameterValues("createMenuItem");
        if (items != null) {
            Set<Item> itemSet = new HashSet<>();
            for (String itemIdString : items) {
                int itemId = Integer.parseInt(itemIdString);
                Item item = itemDao.getItemById(itemId);
                itemSet.add(item);
            }
            if (menuDao.getMenuFromDayAndServingType(day, servingType) == null) {
                menuDao.addMenu(new Menu(day, itemSet, servingType));
            } else {
                Menu menu = menuDao.getMenuFromDayAndServingType(day, servingType);
                menuDao.updateMenu(menu, itemSet);
            }
            servletContext.setAttribute("items", itemDao.getAllItems());
            servletContext.setAttribute("menu", menuDao.getWeeklyMenu());
        }
        return Helper.MANIPULATE_MENUS_URL;
    }

    @RequestMapping(value = "/servlets/admin/DeleteItemServlet")
    public String deleteItem(HttpServletRequest request) {
        String itemIdString = request.getParameter("itemId");
        if (itemIdString != null) {
            int itemId = Integer.parseInt(itemIdString);
            ItemDao itemDao = new ItemDao();
            MenuDao menuDao = new MenuDao();
            Item item = itemDao.getItemById(itemId);
            itemDao.deleteItem(item);
            servletContext.setAttribute("items", itemDao.getAllItems());
            servletContext.setAttribute("menu", menuDao.getWeeklyMenu());
        }
        return Helper.MANIPULATE_ITEMS_URL;
    }

    @RequestMapping(value = "/servlets/admin/DeleteMenuServlet")
    public String deleteMenu(HttpServletRequest request) {
        ItemDao itemDao = new ItemDao();
        MenuDao menuDao = new MenuDao();
        int day = Integer.parseInt(request.getParameter("deleteMenuDay"));
        int servingType = Integer.parseInt(request.getParameter("deleteMenuServingType"));
        Menu menu = menuDao.getMenuFromDayAndServingType(day, servingType);
        if (menu != null) {
            menuDao.deleteMenu(menu);
        }
        servletContext.setAttribute("items", itemDao.getAllItems());
        servletContext.setAttribute("menu", menuDao.getWeeklyMenu());
        return Helper.MANIPULATE_MENUS_URL;
    }

    @RequestMapping(value = "/servlets/admin/GetNewItemNameServlet", method = RequestMethod.POST)
    public String goToGetNewItemName() {
        return Helper.GET_NEW_ITEM_NAME_URL;
    }

    @RequestMapping(value = "/servlets/admin/UpdateItemServlet", method = RequestMethod.POST)
    public String updateItem(HttpServletRequest request) {
        String itemIdString = request.getParameter("itemId");
        if (itemIdString != null) {
            int itemId = Integer.parseInt(itemIdString);
            String newItemName = request.getParameter("newItemName");
            ItemDao itemDao = new ItemDao();
            MenuDao menuDao = new MenuDao();
            Item item = itemDao.getItemById(itemId);
            itemDao.updateItem(item, newItemName);
            servletContext.setAttribute("items", itemDao.getAllItems());
            servletContext.setAttribute("menu", menuDao.getWeeklyMenu());
        }
        return Helper.MANIPULATE_ITEMS_URL;
    }
}
