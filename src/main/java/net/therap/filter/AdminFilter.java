package net.therap.filter;

import net.therap.domain.User;
import net.therap.helper.Helper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/28/17
 */

@WebFilter(urlPatterns = {"/servlets/admin/*"})
public class AdminFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            User user = (User) httpServletRequest.getSession().getAttribute("user");
            if (!user.getType().trim().equals(Helper.USER_TYPE_ADMIN)) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher(Helper.HOME_URL);
                requestDispatcher.forward(request, response);
            }
            filterChain.doFilter(request, response);
        }
    }

    public void destroy() {

    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
