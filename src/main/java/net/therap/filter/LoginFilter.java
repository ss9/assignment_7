package net.therap.filter;

import net.therap.domain.User;
import net.therap.helper.Helper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/26/17
 */

@WebFilter(urlPatterns = {"/servlets/*"})
public class LoginFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            User user = (User) httpServletRequest.getSession().getAttribute("user");
            httpServletResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            httpServletResponse.setHeader("Pragma", "no-cache");
            httpServletResponse.setDateHeader("Expires", 0);
            if (user == null) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher(Helper.INDEX_URL);
                requestDispatcher.forward(request, response);
            } else {
                if (!user.getType().trim().equals(Helper.USER_TYPE_ADMIN) &&
                        httpServletRequest.getRequestURL().toString().contains("/admin/")) {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher(Helper.INDEX_URL);
                    requestDispatcher.forward(request, response);
                }
                if (!user.getType().trim().equals(Helper.USER_TYPE_PUBLIC) &&
                        httpServletRequest.getRequestURL().toString().contains("/public/")) {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher(Helper.INDEX_URL);
                    requestDispatcher.forward(request, response);
                }
                filterChain.doFilter(request, response);
            }
        }
    }

    public void destroy() {

    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
